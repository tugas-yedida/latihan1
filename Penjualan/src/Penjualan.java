import java.util.Scanner;

public class Penjualan {
    private String kode;
    private String nama;
    private float harga;
    private int jumlah;

    public void setData(String kode, String nama, float harga, int jumlah) {
        this.kode = kode;
        this.nama = nama;
        this.harga = harga;
        this.jumlah = jumlah;
    }

    public float getTotalPembelian() {
        return harga * jumlah;
    }

    public String getBonus() {
        float totalPembelian = getTotalPembelian();
        if (totalPembelian >= 500000 || jumlah > 5) {
            return "Setrika";
        } else if (totalPembelian >= 100000 || jumlah > 3) {
            return "Payung";
        } else if (totalPembelian >= 50000 || jumlah > 2) {
            return "Ballpoint";
        } else {
            return "Tidak ada bonus";
        }
    }

    public void cetakNota() {
        System.out.println("Kode            : " + kode);
        System.out.println("Nama            : " + nama);
        System.out.println("Harga           : " + harga);
        System.out.println("Jumlah          : " + jumlah);
        System.out.println("Total Pembelian : " + getTotalPembelian());
        System.out.println("Bonus           : " + getBonus());
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Penjualan penjualan = new Penjualan();
        String inputLagi;
        do {
            System.out.print("Masukkan kode   : ");
            String kode = scanner.nextLine();
            System.out.print("Masukkan nama   : ");
            String nama = scanner.nextLine();
            System.out.print("Masukkan harga  : ");
            float harga = scanner.nextFloat();
            System.out.print("Masukkan jumlah : ");
            int jumlah = scanner.nextInt();
            scanner.nextLine();

            penjualan.setData(kode, nama, harga, jumlah);
            penjualan.cetakNota();

            System.out.print("Input data lagi [Y/T]? ");
            inputLagi = scanner.nextLine();
        } while (inputLagi.equalsIgnoreCase("Y"));
    }
}